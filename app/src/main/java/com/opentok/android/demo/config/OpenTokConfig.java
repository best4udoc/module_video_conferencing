package com.opentok.android.demo.config;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID
    public static final String SESSION_ID = "1_MX40NTU1NDIyMn5-MTQ2MDAyODk5NzQwNX5WMktMZFB3R09GU2U4c3JkV0tBcVA4YWZ-UH4";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NTU1NDIyMiZzaWc9YjFiMWU5N2ZhMmU2NDFmOTYyMmFhODBiNDYyMTk1NmM4ZjljMzIwNDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5UVTFOREl5TW41LU1UUTJNREF5T0RrNU56UXdOWDVXTWt0TVpGQjNSMDlHVTJVNGMzSmtWMHRCY1ZBNFlXWi1VSDQmY3JlYXRlX3RpbWU9MTQ2MDAyOTAwMCZub25jZT0wLjM2NTEzNDAxNDIwNTcxNzgmZXhwaXJlX3RpbWU9MTQ2MjYyMDc3MSZjb25uZWN0aW9uX2RhdGE9YmVzdDR1ZG9jJTQwZ21haWwuY29t";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45554222";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = true;


}
